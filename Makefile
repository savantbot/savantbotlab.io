build: tscompile input/*.md layouts/*.html
	node js/index.js

tscompile: node_modules src/*.ts
	tsc -p tsconfig.json

node_modules: package.json
	npm install

.PHONY: build
