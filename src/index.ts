var Metalsmith  = require('metalsmith');
var markdown    = require('metalsmith-markdown');
var layouts     = require('metalsmith-layouts');
var permalinks  = require('metalsmith-permalinks');

// export class MetalsmithSiteModule {
//   constructor() {
//     console.log('MetalsmithSiteModule loaded....');
//   }
// }


Metalsmith(__dirname)
  .metadata({
    title: "savant-bot-website",
    description: "My GitLab project page.",
    generator: "Metalsmith",
    url: "http://gitlab.com/yofactory/metalsmith-gitlab-pages"
  })
  .source('../input')
  .destination('../public')
  .clean(false)
  .use(markdown())
  .use(permalinks())
  .use(layouts({
    engine: 'handlebars',
    directory: '../layouts'
  }))
  .build(function(err, files) {
    if (err) { throw err; }
  });
